#ENTREGA CONVOCATORIA ENERO. 
Lidia de Castro Rodríguez  -> correo:  l.decastror.2023@alumnos.urjc.es 
enlance: https://youtu.be/b_bLBcyuCik

"REQUISITOS MINIMOS"
- Método change colors
- Método rotate right
- Método mirror
- Método rotate_colors
- Método blur
- Método shift 
- Método crop
- Método greyscale 
- Método filter


"REQUISITOS OPCIONALES"
- Filtros avanzados: Método sepia y método change brightness
- Programa interactivo: transforms_interactive

COMENTARIO: El tiempo máximo que nos da, no da tiempo para explicar del todo el proyecto entero, por lo que lo he intentado explicar brevemente y como he podido.  
