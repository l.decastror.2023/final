from images import read_img, write_img, size, create_blank
import sys

#dividir los colores en rojo, verde, azul
def extract_red(filename: str) -> None:
    pixels = read_img(filename)

    width, height = size(pixels)
    new_pixels = create_blank(width, height)

    for x in range (width):
        for y in range(height):
            new_pixels[x] [y] = (pixels[x] [y] [0], 0, 0)
    name =filename.split(".")[0] + "_rojo"
    ext = filename.split(".") [1]
    new_filename = name + "." + ext

    write_img(new_pixels, new_filename)


def extract_green(filename: str) -> None:
    pixels = read_img(filename)

    width, height = size(pixels)
    new_pixels = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            new_pixels[x][y] = (0, pixels[x][y][1], 0)
    name = filename.split(".")[0] + "_verde"
    ext = filename.split(".")[1]
    new_filename = name + "." + ext
    write_img(new_pixels, new_filename)

def extract_blue(filename: str) -> None:
    pixels = read_img(filename)

    width, height = size(pixels)
    new_pixels = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            new_pixels[x][y] = (0, pixels[x][y][1], 0)
    name = filename.split(".")[0] + "_verde"
    ext = filename.split(".")[1]
    new_filename = name + "." + ext

    write_img(new_pixels, new_filename)


def main():
    if len(sys.argv) == 2:
        filename = sys.argv[1]
        extract_red(filename)
        extract_green(filename)
        extract_blue(filename)
    else:
        print(f"\n[!] Uso: /sys.argv[0] <filename>")


if __name__ == '__main__':
    main()

