from transforms import *
from images import read_img, write_img


pixels = read_img("cafe.jpg")
new_pixels = change_brightness(pixels, 56)
write_img(new_pixels, "cafe_change_brightness.jpg")