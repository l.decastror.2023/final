import sys
from images import read_img, write_img
from transforms import *

transforms_n_args = {
    "mirror": 0,
    "blur": 0,
    "rotate_right": 0,
    "grayscale": 0,
    "change_colors": 6,
    "shift": 2,
    "rotate_colors": 1,
    "filter": 3,
    "crop": 4
}

def get_new_filename(filename: str) -> str:
    name = ".".join(filename.split(".")[:-1]) + "_trans"
    ext = filename.split(".")[-1]
    new_filename = name + "." + ext

    return new_filename

def apply_transform(pixels, transforms):
    transform = transforms[0]
    args = [int(i) for i in transforms[1:]]

    if transforms_n_args[transform] == len(args):
        if transform == "rotate_right":
            new_pixels = rotate_right(pixels)
        elif transform == "mirror":
            new_pixels = mirror(pixels)
        elif transform == "blur":
            new_pixels = blur(pixels)
        elif transform == "grayscale":
            new_pixels = grayscale(pixels)
        elif transform == "change_colors":
            new_pixels = change_colors(pixels, (args[0], args[1], args[2]), (args[3], args[4], args[5]))
        elif transform == "shift":
            new_pixels = shift(pixels, args[0], args[1])
        elif transform == "rotate_colors":
            new_pixels = rotate_colors(pixels, args[0])
        elif transform == "filter":
            new_pixels = filter(pixels, args[0], args[1], args[2])
        elif transform == "crop":
            new_pixels = crop(pixels, args[0], args[1], args[2], args[3])
        return new_pixels
    else:
        print("[!] Numero incorrecto de argumentos")
        sys.exit(1)


def main():
    if len(sys.argv) < 3:
        print(f"\n[!] Uso: {sys.argv[0]} <imagen> <transform>")
        sys.exit(1)
    else:
        filename = sys.argv[1]
        transforms = sys.argv[2:]

        transform = transforms[0]
        if transform not in transforms_n_args:
            print("Transformacion incorrecta")
            sys.exit(1)

        pixels = read_img(filename)
        new_pixels = apply_transform(pixels, transforms)
        new_filename = get_new_filename(filename)
        write_img(pixels, new_filename)

        print("[+] Transformación exitosa!")


if __name__ == '__main__':
    main()