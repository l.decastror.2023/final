import sys
from transforms import *
from images import read_img, write_img
from os import name, system
from time import sleep

RESET = "\033[0m"
ROJO = "\033[91m"
VERDE = "\033[92m"
AMARILLO = "\033[93m"
AZUL = "\033[94m"
MAGENTA = "\033[95m"
CIAN = "\033[96m"
BLANCO = "\033[97m"

def printc(mensaje, color):
    print(color + mensaje + RESET)


def clear_screen():
    if name == 'nt':
        system("cls")
    else:
        system("clear")


def print_menu_principal():
    sleep(2)
    clear_screen()
    print("Introduce una opcion entre las siguientes: ")
    print()
    print("\t0. Salir del programa")
    print("\t1. Leer imagen a trasformar")
    print("\t2. Guardar imagen en archivo")
    print("\t3. Aplicar transformacion")

def print_menu_leer_imagen():
    sleep(2)
    clear_screen()
    print("Introduce una opcion entre las siguientes: ")
    print()
    print("\t0. Salir del programa")
    print("\t1. Leer image a trasformar")

def leer_imagen():
    try:
        filename = input("Introduce el nombre de la imagen a tranformar: ")
        pixels = read_img(filename)
        return pixels
    except FileNotFoundError:
        print( "La imagen no exite!")
        sleep(1)
        leer_imagen()


def guardar_imagen(pixels: List[List[Tuple[int, int, int]]]):
    filename = input("Introduce el nombre del archivo al que quieres guardar la imagen: ")
    write_img(pixels, filename)

def print_menu_transformaciones():
    print("======TRANSFORMATIONS ======")
    print("1. Change colors")
    print("2. Rotate right")
    print("3. Mirror")
    print("4. Rotate colors")
    print("5. Blur")
    print("6. Shift")
    print("7. Crop")
    print("8. Grayscale")
    print("9. Filter")
    print("10. Transform_to_color")
    print("11. Sepia")
    print("12. Change brigthness")


def do_change_colors(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    print()
    print("Introduce los valores del color que quieres cambiar")
    r1 = int(input("Introduce el valor R: "))
    g1 = int(input("Introduce el valor G: "))
    b1 = int(input("Introduce el valor B: "))

    print("Introduce los valores del color al que quieres cambiar")
    r2 = int(input("Introduce el valor R: "))
    g2 = int(input("Introduce el valor G: "))
    b2 = int(input("Introduce el valor B: "))

    new_pixels = change_colors(pixels, (r1, g1, b1), (r2, g2, b2))
    return new_pixels

def do_rotate_right(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    new_pixels = rotate_right(pixels)
    return new_pixels

def do_mirror(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    new_pixels = mirror(pixels)
    return new_pixels

def do_rotate_colors(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    print()
    increment = int(input("Introduce el incremento: "))

    new_pixels = rotate_colors(pixels, increment)
    return new_pixels


def do_blur(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    new_pixels = blur(pixels)
    return new_pixels


def do_shift(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    print()
    desp_hor = int(input("Introduce el desplazamiento horizontal: "))
    desp_ver = int(input("Introduce el desplazamiento vertical: "))

    new_pixels = shift(pixels, desp_hor, desp_ver)
    return new_pixels


def do_crop(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    print()
    print("Introduce los valores del rectangulo a recortar")
    x = int(input("Introduce la columna de la esquina superior izquierda: "))
    y = int(input("Introduce la fila de la esquina superior izquierda: "))
    width = int(input("Introduce la anchura: "))
    height = int(input("Introduce la altura: "))

    new_pixels = crop(pixels, x, y, width, height)
    return new_pixels
def do_grayscale(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    new_pixels = grayscale(pixels)
    return new_pixels

def do_filter(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    print()
    r = float(input("Introduce el multiplicador para R: "))
    g = float(input("Introduce el multiplicador para G: "))
    b = float(input("Introduce el multiplicador para B: "))

    new_pixels = filter(pixels, r, g, b)
    return new_pixels

def do_transform_to_color(pixels: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    new_pixels = transform_to_color(pixels)
    return new_pixels

def do_sepia(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_pixels = sepia(pixels)
    return new_pixels

def do_change_brightness(pixels: List[List[Tuple[int, int, int]]])-> List[List[Tuple[int, int, int]]]:
    print()
    change = float(input("Introduce el multiplicador (>1 para aumentar el brillo, <1 para disminuirlo"))
    new_pixels = change_brightness(pixels, change)
    return new_pixels


def aplicar_transformacion(pixels: List[List[Tuple[int, int, int]]]):
    print_menu_transformaciones()  # primero impreme el penu, dependiendo del numero de ejecucion ejecuta una u otra
    try:
        opcion = int(input("Introduce una transformacion:"))
        if opcion == 1:
            new_pixels = do_change_colors(pixels)
        elif opcion == 2:
            new_pixels = do_rotate_right(pixels)
        elif opcion == 3:
            new_pixels = mirror(pixels)
        elif opcion == 4:
            new_pixels = do_rotate_colors(pixels)
        elif opcion == 5:
            new_pixels = do_blur(pixels)
        elif opcion == 6:
            new_pixels = do_shift(pixels)
        elif opcion == 7:
            new_pixels = do_crop(pixels)
        elif opcion == 8:
            new_pixels = do_grayscale(pixels)
        elif opcion == 9:
            new_pixels = do_filter(pixels)
        elif opcion == 10:
            new_pixels = do_transform_to_color(pixels)
        elif opcion == 11:
            new_pixels = do_sepia(pixels)
        elif opcion == 12:
            new_pixels = do_change_brightness(pixels)

        return new_pixels

    except ValueError:
        print("No has introducido un entero!!")
        sleep(1)
        aplicar_transformacion(pixels)
    except UnboundLocalError:
        print("Opcion invalida!")
        sleep(1)
        aplicar_transformacion(pixels)

def main():
    SALIR = 0
    opcion = -1
    pixels = []
    new_pixels = []
    while opcion != SALIR:
        # si la imagen se ha leido
        if pixels != []:
            print_menu_principal()
            try:
                opcion = int(input(AMARILLO + "Introduce una opcion: " + RESET))

                if opcion == 1:
                    pixels = leer_imagen()
                    new_pixels = pixels
                elif opcion == 2:
                    guardar_imagen(new_pixels)
                elif opcion == 3:
                    new_pixels = aplicar_transformacion(new_pixels)

            except ValueError:
                printc("Introduce un numero entero!", ROJO)

        else:
            print_menu_leer_imagen()

            try:
                opcion = int(input(AMARILLO + "Introduce una opcion: " + RESET))
                if opcion == 1:
                    pixels = leer_imagen()
                    new_pixels = pixels
            except ValueError:
                printc("Introduce un numero entero!", ROJO)

if __name__ == '__main__':
        main()
