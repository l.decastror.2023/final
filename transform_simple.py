from images import read_img, write_img
from transforms import rotate_right, mirror, blur, grayscale
import sys

def get_new_filename (filename: str) -> str:
    name = ".".join(filename.split(".")[:-1]) + "_trans"
    ext = filename.split(".")[-1]
    new_filename = name + "." + ext

    return new_filename

def valid_transform(transform: str) -> bool:
    valid = (transform == "rotate_right" or transform == "mirror" or transform == "blur" or transform == "grayscale")
    return valid

def main():
    if len(sys.argv) < 3:
        print(f"\n[!] Uso: {sys.argv[0]} <imagen> <rotate_right|mirror|blur|grayscale>")
        sys.exit(1)
    else:
        filename = sys.argv[1]
        transforms = sys.argv[2:]

        pixels = read_img(filename)
        new_pixels = pixels

        for transform in transforms:
            if not (valid_transform(transform)):
                print("\nUnknown transform!!")
                print(f"\n[!] Uso: {sys.argv[0]}<imagen> <rotate_right|mirror|blur|grayscale>")
                sys.exit(1)
            else:
                if transform == "rotate_right":
                    new_pixels = rotate_right(new_pixels)
                elif transform == "mirror":
                    new_pixels = mirror(new_pixels)
                elif transform == "blur":
                    new_pixels = blur(new_pixels)
                else:
                    new_pixels = grayscale(pixels)
        new_filename = get_new_filename(filename)
        write_img(new_pixels, new_filename)
        print("[+] Transformación exitosa!")


if __name__ == '__main__':
    main()



