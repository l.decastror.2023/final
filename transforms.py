from images import create_blank, size
from typing import List, Tuple


def change_colors(image: List[List[Tuple[int, int, int]]], to_change: Tuple, to_change_to: Tuple) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            if image[x][y] == to_change:
                new_image[x][y] = to_change_to
            else:
                new_image[x][y] = image[x][y]
    return new_image

def rotate_right(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    original_width, original_height = size(image)
    new_width, new_height = original_height, original_width
    new_image = create_blank(new_width, new_height)

    for x in range(new_width):
        for y in range(new_height):
            #x -> col
            # y -> fila

            # x= ultima        ----> ? = 0
            # x= ultima - 1    ----> ? = 1
            # x= 2             ----> ? = ultima fila - 2
            # x= 1             ----> ? = ultima fila - 1
            # x= 0             ----> ? = ultima fila
            new_image[x][y] = image[y][original_height - 1 - x]

    return new_image


def mirror(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)
    # c = 0 ---> x = ultima col
    # c = 1 ---> x = ulitma col - 1
    #
    # x = 0 --> c = ultima col
    for x in range(width):
        for y in range(height):
            new_image[x][y] = image[width - 1 - x][y]

        return new_image

def rotate_colors(image: List[List[Tuple[int, int, int]]], increment: int) -> List[List[Tuple[int, int, int]]]:
            width, height = size(image)
            new_image = create_blank(width, height)

            for x in range(width):
                for y in range(height):
                    original_pixel = image[x][y]  # tupla (R, G, B)
                    new_pixel = ((original_pixel[0] + increment) % 256, (original_pixel[1] + increment) % 256, (original_pixel[2] + increment) % 256)
                    new_image[x][y] = new_pixel

            return new_image

def calculate_neighbours(x: int, y: int, width: int, height: int) -> List[Tuple[int, int]]:
    neighbours = []
    final_neighbours = []
    neighbours.append((x, y))
    neighbours.append((x - 1, y - 1))
    neighbours.append((x - 1, y))
    neighbours.append((x - 1, y + 1))
    neighbours.append((x, y - 1))
    neighbours.append((x, y + 1))
    neighbours.append((x + 1, y - 1))
    neighbours.append((x + 1, y))
    neighbours.append((x + 1, y + 1))

    for n in neighbours:
        # n es una tupla
        if not ((n[0]< 0) or (n[1] < 0) or (n[0]>= width) or (n[1] <= height)):
            final_neighbours.append(n)

    return final_neighbours

def blur(image:List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            neighbours = calculate_neighbours(x, y, width, height)
            mean_r = 0
            mean_g = 0
            mean_b = 0

            for n in neighbours:
                pixel = image[n[0]][n[1]]
                mean_r += pixel[0]
                mean_g += pixel[1]
                mean_b += pixel[2]

            mean_r = round(mean_r / len(neighbours))
            mean_g = round(mean_g / len(neighbours))
            mean_b = round(mean_b / len(neighbours))
            new_image[x][y] = (mean_r, mean_g, mean_b)

    return new_image

def shift(image:List[List[Tuple[int, int, int]]], horizontal: int, vertical: int) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    #horizontal > 0 ---> derecha
    #vertical < 0 --> arriba

    for x in range(width):
        for y in range(height):
            new_image[x][y] = image[(x- horizontal) % width][(y + vertical) % height]

    return new_image


def crop(image: List[List[Tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> List[List[Tuple[int, int, int]]]:
    new_image = create_blank(width, height)

    for c in range(width):
        for r in range(height):
            new_image[c][r] = image[x+c][y+r]

    return new_image

def grayscale(image:List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            pixel = image[x][y]
            mean = (pixel[0] + pixel[1] + pixel[2]) // 3
            new_image[x][y] = (mean, mean, mean)

    return new_image

def filter(image:List[List[Tuple[int, int, int]]], r: float, g: float, b: float) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            pixel = image[x][y]
            red = pixel[0] * r
            if red > 255:
                red = 255

            green = pixel[1] * g
            if green > 255:
                green = 255

            blue = pixel[2] * b
            if blue > 255:
                blue = 255

            new_image[x][y] = (round(red), round(green), round(blue))


    return new_image

def sepia(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            pixel = image[x][y]
            original_r = pixel[0]
            original_g = pixel[1]
            original_b = pixel[2]
            sepia_r = round(0.393*original_r + 0.769*original_g + 0.189*original_b)
            sepia_g = round(0.349*original_r + 0.686*original_g + 0.168*original_b)
            sepia_b = round(0.272*original_r + 0.534*original_g + 0.131*original_b)
            pixel_sepia = (sepia_r, sepia_g, sepia_b)
            pixel_sepia_lista = list(pixel_sepia)
            for i in range(len(pixel_sepia)):
                if pixel_sepia_lista[i] < 0:
                    pixel_sepia_lista[i] = 0
                elif pixel_sepia_lista[i] > 255:
                    pixel_sepia_lista[i] = 255
            new_image[x][y] = pixel_sepia

    return new_image

def transform_to_color(image: List[List[Tuple[int, int, int]]], color: Tuple[int, int, int]) -> List[List[Tuple[int, int, int]]]:
    new_image = filter(image, color[0] / 255, color[1]/ 255, color[2] / 255)
    return new_image


def change_brightness(pixels: List[List[Tuple[int, int, int]]], change: float) -> List[List[Tuple[int, int, int]]]:
    new_pixels = filter(pixels, change, change, change)
    return new_pixels

